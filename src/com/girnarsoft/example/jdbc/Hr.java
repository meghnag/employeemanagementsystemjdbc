package com.girnarsoft.example.jdbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class Hr {
	int hrMenuChoice;

	/**
	 * This method creates welcome message for HR.
	 * HR can choose his desired option from the list.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void hrMenu() throws ClassNotFoundException, IOException {	
		Scanner in = new Scanner(System.in);
		try
		{
			System.out.println("Hello, HR");
			System.out.println("What would you like to perform?"); 
			System.out.print("1. View All Employees List \n2. Add employee \n3. Remove employee \n4. Promote an employee \n5. Search Employee \n6. Logout");
			hrMenuChoice = in.nextInt();
			tasksHr(hrMenuChoice);
		}
		catch(Exception exception)
		{
		  System.out.println("This is not an integer");
		  hrMenu();
		}finally {
			in.close();
		}
	}
	
	/**
	 * This class inputs the desired task from HR and directs to the specific task method.
	 * @param hrMenuChoice						Integer: This is the task chosen by HR.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	protected void tasksHr(int hrMenuChoice) throws IOException, ClassNotFoundException, SQLException {	
		switch(hrMenuChoice){
		case 1:
			Jdbc.viewEmployee();
			hrMenu();
			break;
		case 2:
			Jdbc.addEmployee();
			hrMenu();
			break;
		case 3:
			Jdbc.removeEmployee();
			hrMenu();
			break;
		case 4:
			Jdbc.promoteEmployee();
			hrMenu();
			break;
		case 5:
		    Jdbc.searchEmployee();
			hrMenu();
			break;
		case 6:
			Jdbc.connectionClose();
			System.out.println("You've been successfully logged out");
			return;
		default:
			System.out.println("Please enter a valid option");
			hrMenu();
			break;
		} //end of switch
	} //end of method tasksHr
} //end of class Hr