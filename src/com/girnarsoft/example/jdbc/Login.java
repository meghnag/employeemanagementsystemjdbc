package com.girnarsoft.example.jdbc;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/** 
 * A JAVA class for user to enter in the system
 * after successful verification.
 * Note: Please use employee ID: 1000
 * 		 and password: Password
 */
public class Login {
	public static void login() throws ClassNotFoundException, FileNotFoundException, IOException {
	String empId = "1000";
	Scanner input = new Scanner(System.in);
	String userId;
	String pass;
	System.out.println("Welcome!");
	
	//validating EmployeeId
	do 
	{
		System.out.println("Enter your numeric Employee Id: ");
		userId = input.next();
	}while(!isNumber(userId));
	
	//input password
	System.out.println("Enter your Password: ");
	pass = input.next();
	
	//checking login details
	if ((userId.equals(empId)) && (pass.equals("Password"))) {
		System.out.println("Welcome to Girnar Softwares");
		EmployeeMenu employeeMenu = new EmployeeMenu();
		employeeMenu.employeeMenu();
	    }
	else
		System.out.println("Invalid login credentials");
	}
	
	/**
	 * 
	 * @param s		String to examine
	 * @return 		boolean: True, if only numbers are present.
	 * 						 False, otherwise.	
	 */
	//method to check whether a string contains digit only
	static boolean isNumber(String s)
    {
        for (int i = 0; i < s.length(); i++)
        if (Character.isDigit(s.charAt(i)) == false)
            return false;
 
        return true;
    }
	
}
