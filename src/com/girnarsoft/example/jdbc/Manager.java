package com.girnarsoft.example.jdbc;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class Manager {
	int managerMenuChoice;
	
	/**
	 * This method creates welcome message for Manager.
	 * Manager can choose his desired option from the list.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void managerMenu() throws ClassNotFoundException, IOException {	
		Scanner in = new Scanner(System.in);
		try
		{
			System.out.println("Hello, Manager");
			System.out.println("What would you like to perform?"); 
			System.out.print("1. View your team \n2. Remove Employee \n3. Logout");
			managerMenuChoice = in.nextInt();
			tasksDirector(managerMenuChoice);
		}
		catch(Exception exception)
		{
		  System.out.println("This is not an integer");
		  managerMenu();
		}finally {
			in.close();
		}
	}
	
	/**
	 * This class inputs the desired task from Manager and directs to the specific task method.
	 * @param managerMenuChoice						Integer: This is the task chosen by Manager.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	protected void tasksDirector(int managerMenuChoice) throws IOException, ClassNotFoundException, SQLException {
		switch(managerMenuChoice)
		{
		case 1:
			Jdbc.viewTeam();
			managerMenu();
			break;
		case 2:
			Jdbc.removeEmployee();
			managerMenu();
			break;
		case 3:
			Jdbc.connectionClose();
			System.out.println("You've been successfully logged out");
			return;
		default:
			System.out.println("Please enter a valid option");
			managerMenu();
			break;
		}
	}
}
