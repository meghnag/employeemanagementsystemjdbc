package com.girnarsoft.example.jdbc;

/**
 *
 * A Java class meant to provide easy and consistent validation for user-generated input.
 */

import java.util.Scanner;
public class Validations {

	/**
	 * A method to repeatedly ask the user for input until 
	 * the input is valid. If condition is used, 
	 * input is measured against it.
	 * 
	 * @param informationText	The information text to prompt
	 * 							to the user.
	 * @return					Returns the final value of the accepted
	 * 							input, as an integer.
	 */
	public int askInputInt() {
		Scanner sc = new Scanner(System.in);
		int number;
		do {
		    while (!sc.hasNextInt()) {
		        System.out.println("That's not a number! Please enter a positive number.");
		        sc.next();
		    }
		    number = sc.nextInt();
		} while (number <= 0);
		sc.close();
		return number;
	}

	/**
	 * A method to repeatedly ask the user for input until 
	 * the input is valid. If condition is used, 
	 * input is measured against it.
	 * 
	 * @param informationText	The information text to prompt
	 * 							to the user.
	 * @return					Returns the final value of the accepted
	 * 							input, as a double.
	 */
	public long askInputLong() {
		Scanner sc = new Scanner(System.in);
		long number;
		do {
		    System.out.println("Please enter a positive number!");
		    while (!sc.hasNextLong()) {
		        System.out.println("That's not a number!");
		        sc.next();
		    }
		    number = sc.nextLong();
		} while (number <= 0);
		sc.close();
		return number;
	}

	/**
	 * A method to repeatedly ask the user for input until 
	 * the input is valid. If condition is used, 
	 * input is measured against it.
	 * 
	 * @param informationText	The information text to prompt
	 * 							to the user.
	 * @return					Returns the final value of the accepted
	 * 							input, as a float.
	 */
	public float askInputFloat() {
		Scanner sc = new Scanner(System.in);
		float number;
		do {
		    System.out.println("Please enter a positive number!");
		    while (!sc.hasNextFloat()) {
		        System.out.println("That's not a number!");
		        sc.next();
		    }
		    number = sc.nextFloat();
		} while (number <= 0);
		sc.close();
		return number;
	}

	/**
	 * Tests if a specific input can be converted to a specific type.
	 * 
	 * @param input The input to test. Accepts String, int, double or float.
	 * @param type	Which type to test against. Accepts 'int','float' or 'double'.
	 * @return Boolean	True if can be transformed to requested type. False otherwise.
	 */
	public Boolean isType(String testStr, String type) {
		try {
			if (type.equalsIgnoreCase("float")) {
				Float.parseFloat(testStr);
			} else if (type.equalsIgnoreCase("int")) {
				Integer.parseInt(testStr);
			} else if (type.equalsIgnoreCase("double")) {
				Double.parseDouble(testStr);
			}
			return true;
		} catch(Exception e) {
			return false;
		}
	}
}
