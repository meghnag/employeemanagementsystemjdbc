package com.girnarsoft.example.jdbc;
/**
 * 
 * A JAVA class to define SQL queries.
 */
public class Constants {

	public static interface Queries {
		public static final String INSERT_EMPLOYEE = "INSERT INTO `ems`.`employee` ("
				+"  `name`,"
				+"  `doj`,"
				+"  `mobile`,"
				+"  `salary`,"
				+"  `password`,"
				+"  `emp_role_id`,"
				+"  `manager_id`,"
				+"  `created_at`,"
				+"  `updated_at`"
				+") "
				+"VALUES"
				+"  ("
				+"?,"
				+"CURDATE(),"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"NOW(),"
				+"NOW()"
				+"  ) ;";
		public static final String REMOVE_EMPLOYEE = "DELETE FROM employee WHERE id = "
				+ "? ";
		public static final String VIEW_EMPLOYEES = "SELECT employee.id, employee.name, role.role_name FROM employee JOIN role ON employee.emp_role_id = role.role_id";
		public static final String PROMOTE_EMPLOYEE = "UPDATE employee SET emp_role_id = "
				+ "?" 
				+ " WHERE id = "
				+ "?";
		public static final String SEARCH_EMPLOYEE = "SELECT name,mobile,emp_role_id,doj FROM employee WHERE id="
				+ "?";
		public static final String VIEW_TEAM = "SELECT id,name FROM employee WHERE manager_id ="
				+ "?";
		public static final String VIEW_TEAM_MANAGER = "SELECT manager_id FROM employee WHERE id ="
				+ "?";
		public static final String UPDATE_MANAGER = "UPDATE employee SET manager_id = "
				+ "?"
				+ " WHERE id = "
				+ "?";
		public static final String LIST_MANAGER = "SELECT id,name FROM employee WHERE emp_role_id ="
				+ "?";
	}

}
