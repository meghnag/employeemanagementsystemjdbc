package com.girnarsoft.example.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * This JAVA class creates connection to our database.
 * This class is also responsible for executing and updating the queries as required.
 *
 * @author Meghna
 *
 */
public class Jdbc {

	public static String JDBC_DRIVER ;
	public static String TARGET_DB_URL;
	public static String OUTPUT_FOLDER_PATH ;
	public static String INFORMATION_DB_URL ;
	public static String USER ;
	public static String PASS ;
	static Connection connection = null;
	public static ResourceBundle bundle = null;
	static Validations validations = new Validations();
	static {

		bundle=  ResourceBundle.getBundle("db");
		JDBC_DRIVER = bundle.getString("JDBC_DRIVER");
		TARGET_DB_URL = bundle.getString("TARGET_DB_URL");
		USER = bundle.getString("USER");
		PASS = bundle.getString("PASS");

		try{
			connection = getDbTxConnection(TARGET_DB_URL, USER, PASS);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 *  This method is called to create a connection.
	 *  It creates a link to our employee database. 
	 *  @param dbUrl				Target Database URL
	 *  @param username				Username of mysql
	 *  @param password				Password of mysql
	 *  */
	public static Connection getDbTxConnection(String dbUrl, String username, String password){
		Connection conn = null;
		try{
			Class.forName(JDBC_DRIVER);
			System.out.println("Connecting to database... ");
			conn = DriverManager.getConnection(dbUrl,username,password);
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}

	/**
	 *  This method adds employee in the company.
	 *  It asks basic details and also assigns role to the employee
	 *  along with his reporting manager.
	 *  @throws SQLException
	 *  */
	public static void addEmployee() throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;
		Scanner scanner = new Scanner(System.in);
		int num = 0;
		String input;
		long longnum = 0;
		float salary = 0;
		int flag = 0;
		try{
			connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.INSERT_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			while(flag==0) {
				try {
				System.out.println("Choose a role for this employee. Please enter a valid integer.");
				System.out.println("1. CEO \n2. Director \n3. HR \n4. Manager \n5.Employee \n");
				num = scanner.nextInt();
				flag=1;
				}catch(Exception e){
					System.out.println("This is an invalid input. Please enter a number.");
				}
			}
			flag=0;
			switch(num) {
			case 1: pstmt.setInt(5, 1); 
			break;
			case 2: pstmt.setInt(5, 2);
			break;
			case 3: pstmt.setInt(5, 3);
			break;
			case 4: pstmt.setInt(5, 4);
			break;
			case 5: pstmt.setInt(5, 5);
			break;
			default: System.out.println("Please enter a valid number from 1 to 5\n");
			addEmployee();	
			}

			System.out.println("Enter name of employee: ");
			input = scanner.next();
			pstmt.setString(1, input);

			while(flag==0) {
				try {
				System.out.println("Enter mobile no.: ");
				longnum = scanner.nextLong();
				flag=1;
				}catch(Exception e){
					System.out.println("This is an invalid input. Please enter a number.");
				}
			}
			flag = 0;
			pstmt.setLong(2, longnum);
		
			while(flag==0) {
				try {
				System.out.println("Enter salary: ");
				salary = scanner.nextFloat();
				flag=1;
				}catch(Exception e){
					System.out.println("This is an invalid input. Please enter a number.");
				}
			}
			flag = 0;
			pstmt.setFloat(3, salary);

			System.out.println("Enter password for this employee: ");
			input = scanner.next();
			pstmt.setString(4, input);
			pstmt.setInt(6, 1);

			int numero = pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			if(rs.next()) {
			System.out.println("Hi employee! This is your generated employee ID:   " + rs.getInt(1));
			listManager(num , rs.getInt(1));
			}
			connection.commit();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}  
		}
	}
	
/**
 * This method displays a possible list of manager for an employee.
 * @param int role Id, the role id of an employee.
 * 
 *  */
	public static void listManager(int roleId, int empId) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;
		Scanner scanner = new Scanner(System.in);
		int num;

		try{
			connection.setAutoCommit(false);
			if(roleId == 1)
			{
				System.out.println("CEO successfully added!");
				return;
			}
			pstmt = connection.prepareStatement(Constants.Queries.LIST_MANAGER, Statement.RETURN_GENERATED_KEYS);
			num = roleId - 1;
			pstmt.setInt(1, num);
			rs = pstmt.executeQuery();
			System.out.println("Choose manager for this employee: ");
			while(rs.next())
			{
				System.out.println("ID: " + rs.getInt(1) + "   NAME: " + rs.getString(2) );
			}
			System.out.println("Choose an id from above: ");
			num = scanner.nextInt();
			updatemanager(num,empId);
			connection.commit();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}   
		}
	}

	/**
	 * This method updates manager for an employee. This will affect the manager_id of the employee table.
	 * @param: int num, the manager ID of an employee.
	 * */
	private static void updatemanager(int num, int empId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
			connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.UPDATE_MANAGER, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, num);
			pstmt.setInt(2, empId);
			//execute update
			int numero = pstmt.executeUpdate();
			System.out.println("Employee successfully added! ");
			connection.commit();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}   
		}
	}
 /**
  * This method searches an employee from the database on the basis of his employee id.
  * Employee name and basic details are displayed.
  *  */
	public static void searchEmployee() throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;
		Scanner scanner = new Scanner(System.in);
		int num = 0;
		int flag = 0;
		try{
			connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.SEARCH_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			while(flag==0) {
				try {
				System.out.println("Enter Employee Id whom you want to search: ");
				num = scanner.nextInt();
				flag=1;
				}catch(Exception e){
					System.out.println("This is an invalid input. Please enter a number.");
				}
			}
			pstmt.setInt(1, num);
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				System.out.println("NAME: " + rs.getString(1) + "   Employee Role ID: " + rs.getInt(3)  );
			}
			connection.commit();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}   
		}
	}

	/**
	 * This method lists down all the employees in the company. It requires no arguments.  */
	public static void viewEmployee() throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.VIEW_EMPLOYEES, Statement.RETURN_GENERATED_KEYS);
			rs = pstmt.executeQuery();
			System.out.println("List of all employees");
			System.out.println("---------------------");
			while(rs.next())
			{
				System.out.println("ID: " + rs.getInt(1) + "     NAME: " + rs.getString(2) + "    Role: " + rs.getString(3));
			}
			connection.commit();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}
		}
	}

	/**
	 * This method deletes the record of an employee from the database on the basis of his employee Id. */
	public static void removeEmployee() throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;
		Scanner scanner = new Scanner(System.in);
		int num = 0;
		int flag=0;
		pstmt = connection.prepareStatement(Constants.Queries.REMOVE_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
		try{
			connection.setAutoCommit(false);
			//numString = scanner.next();
			while(flag==0)
			{
				try {
					System.out.println("Enter employee Id: ");
					num = scanner.nextInt();
					flag = 1;
				}catch(Exception e){
					System.out.println("This is an invalid input.");
				}
			}
			pstmt.setInt(1, num);
			int numero = pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			System.out.println("Employee record deleted");
			connection.commit();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}   
		}
	}

	/** 
	 * This method changes the designation of an employee.
	 *  Designation can be modified if it is used by a person with higher authority.
	 *  */
	public static void promoteEmployee() throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;
		Scanner scanner = new Scanner(System.in);
		int num=0;
		int userEmpId, user2EmpId,newRoleId;

		try{
			connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.PROMOTE_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			System.out.println("Enter your role Id: ");
			userEmpId = scanner.nextInt();
			System.out.println("Enter employee Id whom you want to promote: ");
			num = scanner.nextInt();
			pstmt.setInt(2, num);
			System.out.println("Enter employee's current role Id whom you want to promote: ");
			user2EmpId = scanner.nextInt();
			if(userEmpId > user2EmpId)
			{
				System.out.println("Sorry! You don't have the permissions to configure role for this employee");
				return;
			}
			System.out.println("Choose a role for this employee. Please enter a valid integer.");
			System.out.println("1. CEO \n2. Director \n3. HR \n4. Manager \n5.Employee \n");
			newRoleId = scanner.nextInt();

			switch(newRoleId) {
			case 1: pstmt.setInt(1, newRoleId ); 
			break;
			case 2: pstmt.setInt(1, newRoleId );
			break;
			case 3: pstmt.setInt(1, newRoleId );
			break;
			case 4: pstmt.setInt(1, newRoleId );
			break;
			case 5: pstmt.setInt(1, newRoleId );
			break;
			default: System.out.println("Please enter a valid number from 1 to 5\n");
			addEmployee();	
			}

			int numero = pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			System.out.println("Employee's designation changed.");
			connection.commit();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}  
		}	
	}

	/** 
	 * This method is used by "Manager" and "Basic Employee" role employees in the company.
	 * They can view their teams.
	 * Team here implies a set of people having a common manager.
	 * @throws SQLException
	 *  */
	public static void viewTeam() throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;
		Scanner scanner = new Scanner(System.in);
		int num=0;
		int flag=0;		
		try{
			connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.VIEW_TEAM, Statement.RETURN_GENERATED_KEYS);
			while(flag==0) {
				try {
				System.out.println("Enter your Id: ");
				num = scanner.nextInt();
				flag=1;
				}catch(Exception e){
					System.out.println("This is an invalid input. Please enter a number.");
				}
			}
			pstmt.setInt(1, num);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				System.out.println("Id: " + rs.getInt(1) + "   Name: " + rs.getString(2));
			}
			if(!rs.next())
				System.out.println("No more employees in team currently.");
			connection.commit();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}   
		}	
	}

	/** 
	 * This method is used by "Basic Employee" role employees in the company.
	 * They can view details of their manager.
	 * @throws SQLException
	 * */
	public static void viewManager() throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;
		Scanner scanner = new Scanner(System.in);
		int num = 0;
		int flag = 0;

		try{
			connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(Constants.Queries.VIEW_TEAM_MANAGER, Statement.RETURN_GENERATED_KEYS);
			while(flag==0) {
				try {
				System.out.println("Enter your Id: ");
				num = scanner.nextInt();
				flag=1;
				}catch(Exception e){
					System.out.println("This is an invalid input. Please enter a number.");
				}
			}
			pstmt.setInt(1, num);
			rs = pstmt.executeQuery();
			System.out.println("Your manager details:");
			while(rs.next()) {
				System.out.println("Id: " + rs.getInt(1));
			}
			connection.commit();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}finally{  
			if (pstmt!=null){  
				pstmt.close();  
			}   
		}		
	}
	
	/** 
	 * This method closes the connection with the database.
	 * It is called when a user logs out.
	 *  */
	public static void connectionClose() throws SQLException {
		//if(connection!=null){  
			connection.close(); 
			System.out.println("Connection closed.");
		//}
	}
}
