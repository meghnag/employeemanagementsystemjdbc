package com.girnarsoft.example.jdbc;
import java.io.IOException;
import java.util.Scanner;
/**
 * 
 * A JAVA class which relates an employee with his designation
 * and further directs him to the specific portal.
 *
 */
public class EmployeeMenu {
	int employeeMenuChoice;
	
	/**
	 * A method to relate an employee with his designation.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void employeeMenu() throws ClassNotFoundException, IOException {	
			Scanner in = new Scanner(System.in);
			try
			{
				System.out.println("What role do you identify with?");
				System.out.println("Choose from the options below:");
				System.out.println("1. CEO \n2. Director \n3. HR \n4. Manager \n5. Employee \n6. Logout");
				employeeMenuChoice = in.nextInt();
				chooseOption(employeeMenuChoice);
			}
			catch(Exception exception)
			{
			  System.out.println("This is not an integer");
			  employeeMenu();
			}finally {
				in.close();
			}
	}

	/**
	 * A method to direct an employee to his specific portal
	 * based on his designation.
	 * @param employeeMenuChoice
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void chooseOption(int employeeMenuChoice) throws IOException, ClassNotFoundException {
		switch(employeeMenuChoice)
		{
		case 1:
			Ceo ceo1 = new Ceo();
			ceo1.ceoMenu();
			break;
		case 2:
			Director director1 = new Director();
			director1.directorMenu();
			break;
		case 3:
			Hr hr1 = new Hr();
			hr1.hrMenu();
			break;
		case 4:
			Manager manager1 = new Manager();
			manager1.managerMenu();
			break;
		case 5:
			BasicEmployee basicEmployee1 = new BasicEmployee();
			basicEmployee1.basicEmployeeMenu();
			break;
		case 6:
			System.out.println("You've been successfully logged out");
			return;
		default:
			System.out.println("Please enter a valid option");
			employeeMenu();
		
		}		
	}
}
