package com.girnarsoft.example.jdbc;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class Ceo {
	int ceoMenuChoice;
	
	/**
	 * This method creates welcome message for CEO.
	 * CEO can choose his desired option from the list.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void ceoMenu() throws ClassNotFoundException, IOException {	
		Scanner in = new Scanner(System.in);
		try
		{
			System.out.println("Hello, CEO");
			System.out.println("What would you like to perform?");
			System.out.print("1. View All Employees List \n2. Add employee \n3. Remove employee \n4. Promote an employee \n5. Logout");
			ceoMenuChoice = in.nextInt();
			tasksCeo(ceoMenuChoice);
		}
		catch(Exception exception)
		{
		  System.out.println("This is not an integer");
		  ceoMenu();
		}
	}
	
	/**
	 * This class inputs the desired task from CEO and directs to the specific task method.
	 * @param ceoMenuChoice						Integer: This is the task chosen by CEO.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	protected void tasksCeo(int ceoMenuChoice) throws IOException, ClassNotFoundException, SQLException {
		switch(ceoMenuChoice)
		{
		case 1:
			Jdbc.viewEmployee();
			ceoMenu();
			break;
		case 2:
			Jdbc.addEmployee();
			ceoMenu();
			break;
		case 3:
			Jdbc.removeEmployee();
			ceoMenu();
			break;
		case 4:
			Jdbc.promoteEmployee();
			ceoMenu();
			break;
		case 5:
			Jdbc.connectionClose();
			System.out.println("You've been successfully logged out");
			return;
		default:
			System.out.println("Please enter a valid option");
			ceoMenu();
			break;
		}
	}
}
