package com.girnarsoft.example.jdbc;
import java.io.IOException;

/**
 * 
 * The Ems program implements a system that
 * deals with management of employees in a company.
 * It acts as a portal for CEO,
 * Director, HR, Manager and Employee.
 * The portal provides the information required
 * and helps in executing specific tasks.
 * 
 * @author Meghna
 * @version 2
 */
public class Ems{
	public static void main(String[] args) throws ClassNotFoundException,IOException {
		EmployeeMenu employeeMenu = new EmployeeMenu();
		employeeMenu.employeeMenu();
		//Login.login();
		/* 
		 * Please Note:
		 * UserID: 1000
		 * password: Password
		 */
	}
}
