package com.girnarsoft.example.jdbc;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class Director {
	int directorMenuChoice;
	
	/**
	 * This method creates welcome message for Director.
	 * Director can choose his desired option from the list.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void directorMenu() throws ClassNotFoundException, IOException {	
		Scanner in = new Scanner(System.in);
		try
		{
			System.out.println("Hello, Director");
			System.out.println("What would you like to perform?"); 
			System.out.print("1. View All Employees List \n2. Search Employee \n3. Promote an employee \n4. Logout");
			directorMenuChoice = in.nextInt();
			tasksDirector(directorMenuChoice);
		}
		catch(Exception exception)
		{
		  System.out.println("This is not an integer");
		  directorMenu();
		}finally {
			in.close();
		}
	}
	
	/**
	 * This class inputs the desired task from Director and directs to the specific task method.
	 * @param directorMenuChoice						Integer: This is the task chosen by Director.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	protected void tasksDirector(int directorMenuChoice) throws IOException, ClassNotFoundException, SQLException {
		switch(directorMenuChoice)
		{
		case 1:
			Jdbc.viewEmployee();
			directorMenu();
			break;
		case 2:
			Jdbc.searchEmployee();
			directorMenu();
			break;
		case 3:
			Jdbc.promoteEmployee();
			directorMenu();
			break;
		case 4:
			Jdbc.connectionClose();
			System.out.println("You've been successfully logged out");
			return;
		default:
			System.out.println("Please enter a valid option");
			directorMenu();
			break;
		} //end of switch
	} //end of method tasksDirector
}
