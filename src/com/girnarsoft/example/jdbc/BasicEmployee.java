package com.girnarsoft.example.jdbc;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class BasicEmployee {
	int basicEmployeeMenuChoice;
	
	/**
	 * This method creates welcome message for BasicEmployee.
	 * BasicEmployee can choose his desired option from the list.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected void basicEmployeeMenu() throws ClassNotFoundException, IOException {	
			Scanner in = new Scanner(System.in);
			try
			{
				System.out.println("Hello, Employee");
				System.out.println("What would you like to perform?");
				System.out.print("1. View All Employees List \n2. View team \n3. View manager \n4. Logout");
				basicEmployeeMenuChoice = in.nextInt();
				tasksBasicEmployee(basicEmployeeMenuChoice);
			}
			catch(Exception exception)
			{
			  System.out.println("This is not an integer");
			  basicEmployeeMenu();
			}finally {
				in.close();
			}
	}
	
	/**
	 * This class inputs the desired task from BasicEmployee and directs to the specific task method.
	 * @param basicEmployeeMenuChoice				Integer: This is the task chosen by BasicEmployee.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */	
	protected void tasksBasicEmployee(int basicEmployeeMenuChoice) throws IOException, ClassNotFoundException, SQLException {	
		switch(basicEmployeeMenuChoice)
		{
		case 1:
			Jdbc.viewEmployee();
			basicEmployeeMenu();
			break;
		case 2:
			Jdbc.viewTeam();
			basicEmployeeMenu();
			break;
		case 3:
			Jdbc.viewManager();
			basicEmployeeMenu();
			break;
		case 4:
			System.out.println("You've been successfully logged out");
			return;
		default:
			System.out.println("Please enter a valid option");
			break;
		}
	}
}
