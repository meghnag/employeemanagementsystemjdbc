/* Database: ems */

/* A role table for the company */
CREATE TABLE role (
role_id INT(6) AUTO_INCREMENT PRIMARY KEY, 
role_name VARCHAR(30),
created_at DATETIME,
updated_at DATETIME
)ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* Dumping data for table 'role' */
LOCK TABLES 'role' WRITE;
INSERT INTO role
VALUES (1,'ceo', NOW(), NOW()),
(2,'director', NOW(), NOW()),
(3,'hr', NOW(), NOW()),
(4,'manager', NOW(), NOW()),
(5,'basic_employee', NOW(), NOW()
)
UNLOCK TABLES;

/* An employee table */
CREATE TABLE employee(
id INT(10) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(50) NOT NULL,
doj DATE,
mobile INT(15),
salary FLOAT(20,4),
password VARCHAR(30),
emp_role_id INT(6),
manager_id INT(10),
created_at DATETIME,
updated_at DATETIME
)ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* Dumping data for table 'employee' */
LOCK TABLES `employee` WRITE;
INSERT INTO employee
VALUES (1,'Meghna','2018-06-04',9929508961,100.00,'password',1,1,'2018-06-04 02:08:32', NOW()),
(2,'Ishita','2018-06-04',7073867890,200.00,'password',2,1,'2018-06-04 03:10:42', NOW()),
(3,'Pooja','2018-06-06',5674345061,150.00,'password',3,2,'2018-06-06 04:08:32', NOW()),
(4,'Shashank','2018-06-07',9926708961,300.00,'password',4,3,'2018-06-07 02:08:32', NOW()),
(5,'Shubham','2018-06-07',9789508961,400.00,'password',5,4,'2018-06-07 02:08:32', NOW())

UNLOCK TABLES;

